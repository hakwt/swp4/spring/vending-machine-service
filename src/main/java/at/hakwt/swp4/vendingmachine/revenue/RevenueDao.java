package at.hakwt.swp4.vendingmachine.revenue;

import java.time.LocalDate;

public interface RevenueDao {

    void registerRevenue(double price, LocalDate date);

    Double getDailyRevenue(LocalDate date);

}
